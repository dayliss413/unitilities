﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractEvent<T> where T: AbstractEvent<T>
{
    public delegate void EventDelegate(T EventData);
    private static event EventDelegate globalEvent;
    private event EventDelegate localEvent;

    public static void Register(EventDelegate d)
    {
        globalEvent += d;
    }

    public void RegisterLocal(EventDelegate d)
    {
        localEvent += d;
    }

    public void Unregister(EventDelegate d)
    {
        globalEvent -= d;
    }

    public void UnregisterLocal(EventDelegate d)
    {
        localEvent -= d;
    }

    public void Invoke()
    {
        globalEvent((T)this);
    }

    public void InvokeLocal()
    {
        localEvent((T)this);
    }
}
