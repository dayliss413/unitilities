﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T: Singleton<T>
{
    private static T instance;

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Debug.LogError("More than one instance of " + typeof(T).Name);
        }
        instance = (T)this;
    }

    public static T Instance
    {
        get
        {
            if(instance == null)
            {
                instance = GameObject.FindObjectOfType<T>();
            }
            return instance;
        }
    }

    public void Disable()
    {
        Debug.LogError("Disabling Singleton of " + typeof(T).Name);
    }
}
