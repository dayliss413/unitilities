﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Prop<T>
{
    private static Dictionary<string, T> data = new Dictionary<string, T>();

    public static void Set(string key, T value)
    {
        data[key] = value;
    }

    public static T Get(string key)
    {
        return data[key];
    }

    public static bool TryGet(string key, out T val)
    {
        return data.TryGetValue(key, out val);
    }

    public static bool Has(string key)
    {
        return data.ContainsKey(key);
    }
}
