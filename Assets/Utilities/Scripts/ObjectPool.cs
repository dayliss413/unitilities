﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObjectPool : MonoBehaviour
{
    public GameObject PoolObject;
    private List<GameObject> pooled;

    private void Awake()
    {
        pooled = new List<GameObject>();
    }

    private GameObject makeNew()
    {
        GameObject go = Instantiate(PoolObject);
        pooled.Add(go);
        go.transform.SetParent(transform);
        go.SetActive(false);
        return go;
    }

    public GameObject GetObject()
    {
        int index = pooled.FindIndex(x => x.activeInHierarchy == false);
        if(index < 0)
        {
            return makeNew();
        }
        return pooled[index];
    }
}
