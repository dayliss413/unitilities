﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitialScene : MonoBehaviour
{
    public int InitialSceneIndex;

    private void Awake()
    {
#if UNITY_EDITOR
        if(Prop<int>.Has("InitSceneIndex") == false)
        {
            Prop<int>.Set("InitSceneIndex", InitialSceneIndex);
            SceneManager.LoadScene(InitialSceneIndex);
        }
#else
        SceneManager.LoadScene(InitialSceneIndex);
#endif
    }
}
