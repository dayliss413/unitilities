﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Timer
{
    public float MaxTime;
    public bool Loop = true;
    public float currentTime {get; private set;}
    
    public Timer()
    {
        Reset();
    }

    public Timer(float maxTime, bool loop = true)
    {
        Loop = loop;
        MaxTime = maxTime;
        Reset();
    }

    public void Reset()
    {
        currentTime = MaxTime;
    }

    public bool Tick()
    {
        currentTime -= Time.deltaTime;
        return check();
    }

    public bool Tick(float delta)
    {
        currentTime -= delta;
        return check();
    }

    public bool Check()
    {
        return currentTime <= 0;
    }

    private bool check()
    {
        if(currentTime <= 0)
        {
            if(Loop)
            {
                Reset();
            }
            return true;
        }
        return false;
    }
}
