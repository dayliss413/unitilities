﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Preload : MonoBehaviour
{
    public int PreloadSceneIndex;

    private void Awake()
    {
#if (UNITY_EDITOR)
        if (Prop<int>.Has("InitSceneIndex") == false)
        {
            Prop<int>.Set("InitSceneIndex", SceneManager.GetActiveScene().buildIndex);
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.LoadScene(PreloadSceneIndex);
        }
#endif
        Destroy(gameObject);
    }
    
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        SceneManager.LoadScene(Prop<int>.Get("InitSceneIndex"));
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
